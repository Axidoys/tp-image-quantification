#!/bin/bash

FFPLAY='ffplay.exe'

for file in 'cat.pgm' 'catUniform.pgm' 'catAdaptative.pgm'; do
    $FFPLAY "cmake-build-debug/"$file 2>/dev/null &
    $FFPLAY "cmake-build-debug/"$file -vf histogram 2>/dev/null &
done