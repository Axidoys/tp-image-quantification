#include <stdio.h>
#include "FileFlow.h"
#include "Uniform.h"
#include "Adaptative.h"
#include "FloydSteinberg.h"

void (*functionPerPixel)(int v);

int main() {
    printf("Running the quantification project!\n");

    //Produce uniform quantification in file catUniform.pgm
    functionPerPixel = &uniform_functionPerPixel;
    parseImage("./cat.pgm", "./catUniform.pgm");

    //Color uniform quantification
    functionPerPixel = &uniform_functionPerPixel; //(yes, again, just to be sure)
    parseImage("./dog.ppm", "./dogUniform.ppm");

    //Produce adaptative quantification in file catAdaptative.pgm
    functionPerPixel = &adaptative_fillStats;
    parseImage("./cat.pgm", NULL);
    adaptative_computeStats();

    functionPerPixel = &adaptative_functionPerPixel;
    parseImage("./cat.pgm", "catAdaptative.pgm");

    //Produce with Floyd-Steinberg process over uniform quantification in file cat_floydsteingberg_uniform.pgm
    functionPerPixel = &floydSteinberg_functionPerPixel;
    parseImage("./cat.pgm", "catFloydSteinberg.pgm");

    printf("The quantification project proceed without any problem :-)\n");
    return 0;
}
