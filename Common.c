
#include <stdio.h>
#include "Common.h"

#define CUSTOM_LF_PERIOD 5

extern FILE * fout;
extern int dynRange;

int emulateLargeScale(int vSmall){
    //in place of using a file with a different header, we can keep the same dynamic range but use a portions of the values
    return vSmall * SCALE_FACTOR;
}

void writePixel(int pixelValue) {
    static FILE* fcompare = NULL;
    static int pixCount = -1;
    if(fcompare != fout){//we are now writing to a new file //TODO does not seems to works fine as fout could keep the same value
        fcompare = fout;
        pixCount = 0;
    }
    pixCount++;

    //safeguard
    if(pixelValue > dynRange){
        pixelValue = dynRange;
    }

    if(pixCount % CUSTOM_LF_PERIOD == 0){ //append a new line regularly in order to reduce size of the buffer needed for reading the image
        fprintf(fout, "%d\n", pixelValue);
    }
    else{
        fprintf(fout, "%d ", pixelValue);
    }
}