# Compression par quantification d'image PGM

TP/Projet de Transport d'Information Multimédia dirigé par m. Laurent Grisoni sur la quantification en image.

## Le format plain PGM

Ce format de fichier est simple. PGM est l'acronyme de "Portable GrayMap", ce format de fichier est donc destiné à des images en niveaux de gris. Le préfixe "plain" définit une variante dont la particularité est que le codage des valeurs est codé en ASCII plutôt qu'en binaire (par exemple la valeur 123 est stocké sur 3 octets).

Voilà la forme de ces fichiers :

1. première ligne = "P2" , c'est l'identifiant pour le plain PGM
2. seconde ligne : contient les dimensions largeur puis hauteur
3. troisième ligne : définit la valeur max de chaque pixel
4. chaque nouvelle ligne contient un lot de pixel

Il existe [une bibliothèque C](http://netpbm.sourceforge.net/doc/libnetpbm.html) pour la manipulation de ces fichiers mais le programme est écrit sans celle-ci à des fins pédagogiques.

## Compilation et utilisation

La compilation se fait via Cmake.

Les fichiers d'entrés du programme sont hardcodés dans la fonction main (nécessite d'avoir les fichiers cat.pgm et dog.ppm dans le dossier d'exécution). Mais le programme est flexible et peut recevoir n'importe quelle taille d'image en plain pgm. Les images couleurs ne sont prises en charge que pour la quantification uniforme. Le SCALE_FACTOR est également un paramètre hardcodé.

L'exécutable généré comporte à la fois toutes les parties du TP, elles sont exécutées les unes après les autres. En sortie on a les fichiers `catXXX.pgm` et `dogUniform.pgm` .

Un exécutable Linux est automatiquement compilé à [cet endroit](https://gitlab.com/Axidoys/tp-image-quantification/-/jobs/artifacts/cicd/download?job=build-and-run) avec les pipelines Gitlab, vous pouvez également ne récupérer que le Makefile. Attention, la fonction 'getline' dans `FileFlow.c` entrera en conflit avec celle de la bibliothèque standard dans linux.

## Développement théorique

Ce TP porte sur les méthodes de quantifications d'une image.

Cela peut s'intégrer dans les méthodes de compression avec perte car une quantification plus grossière signifie moins d'informations, et donc des fichiers moins lourds. Dans ce cas la compression/transformation est toujours 'avec pertes', mais il est possible de faire en sorte que l'image captée par les yeux puis interprétée par le cerveau se rapproche au mieux de la source (pour nous c'est une image de base).

### Quantification uniforme

Cette méthode consiste à réduire la plage dynamique en coupant de manière régulière la plage originale comme on le fait sur ce signal : 

![signal quantifié](readme_assets/uniform_quant_wave.png)

Ainsi, sur une image en noir et blanc, l'opération sur chaque valeur de pixel est de la diviser par un certain SCALE_FACTOR.

Si par exemple je veux passer d'une dynamique de 255 à une dynamique de 16 (donc les pixels ne peuvent représenter des valeurs entière que entre 0 et 15), ainsi je vais diviser avec SCALE_FACTOR=16 .

Le programme en C de ce répertoire rend des images d'une plage de 255 (notamment utile pour la quantification adaptative plus bas), mais la quantification est bien émulé car uniquement un sous-ensemble de valeur n'est possible pour les pixels.

Voici le résultat avec dans l'ordre (gauche->droite) : l'image originale, quantification uniforme sur 16 valeur, puis sur 4 valeur.

![les chats uniformes](readme_assets/uniform_cat.PNG)

Nous observons assez facilement ce que signifie la "perte d'information" avec ce résultat ! Même si le chat est toujours reconnaissable, les détails et les reliefs de celui-ci ont disparus. A contrario, les longs dégradés comme on peut voir en arrière-plan, produisent des délimitations "dures" qui se remarque sur une quantification proche de l'original.

Le code écrit prend en charge implicitement les fichiers couleurs pour la quantification uniforme :

![le chien uniforme](readme_assets/dog.PNG)

### Quantification adaptative

Le découpage régulier est une possibilité mais lorsque qu'une partie de la plage n'est pas utilisée nous "gâchons" cet endroit. De manière générale la quantification adaptative c'est donner plus d'importance aux endroits du spectre plus utilisé, et de découper selon ce principe.

Voici le résultat, avec à gauche la quantification uniforme et à droite la quantification adaptative.

![le chat adapté](readme_assets/adaptative_cat.PNG)

Ainsi les zones importantes (statistiquement) comme la partie droite (référenciel de l'écran) retrouve plus de détails et des dégradés mieux définis. Mais l'algorithme est 'bête', et donne aussi de l'importance à l'arrière-plan, ainsi nous perdons dans les hautes lumières les détails de la partie gauche du chat.

On peut voir que l'histogramme est plat, il a été adapté : (haut->bas) original, uniforme, adapté :

![le chat adapté, mais que les histos](readme_assets/adaptative_histo.PNG)

### Quantification avec diffusion d'erreur

La quantification créée une erreur par définition. Si on propage cette erreur spatialement sur les pixels voisins, notre vue floute (re-mélange les pixels) et notre cerveau va reconstruire (malgré lui) l'image originale.

Ainsi on utilise ici l'algorithme de Floyd-Steinberg, et voici le résultat :

![le chat doucement psyko-bruité](readme_assets/floyd.PNG)

Le résultat est bluffant, il est très difficile de voir des artefacts. Même en monochrome l'image est très reconnaissable :

![le chat hautement psyko-bruité](readme_assets/floyd_monochrome.PNG)

Néamoins il faut faire attention au repliement de spectre (et ceci est un avertissement au lecteur, ça justifie que l'image ci-dessus n'apparaisse pas comme elle l'est vraiment sur certains affichages, dans ce cas veuillez ouvrir [l'image originale](readme_assets/floyd_monochrome.PNG)). Voici à quoi peu ressembler l'image dont on a changé sa taille de manière brute : 

![le chat incroyablement psyko-bruité](readme_assets/floyd_monochrome_aliased.PNG)

Il existe d'autres algorithmes de diffusion d'erreur, celui-ci est un des plus utilisé, notamment car il ne rajoute pas d'effet visuel comme des lignes ou des quadrillages. Exemple avec dans FloydSteinberg.c, `COEF1 = 1, COEF2/3/4 = 0` :

![le chat d'une grande lignée](readme_assets/floyd_lined.PNG)

Vous pourrez trouver d'autre d'exemple sur <https://www.arabicprogrammer.com/article/5386388171/>

## Remarque d'implémentation

Une attention particulière a été donné sur l'écriture du code.

La fonction `main` ne sert juste à donner la trame du TP en appelant les procédures pour chaque question du TP.

Pour chaque exercice de quantification, la même fonction de lecture du fichier est utilisé, c'est `parseImage` dans `FileFlow.c` .

Ainsi, l'idée est de faire un traitement "stream-based" (avec des flux).
* Pour la quantification uniforme : la lecture de chaque pixel nous permet d'écrire directement dans le fichier d'output. Rien n'est gardé en mémoire.
* Pour la quantification adaptative : il est nécessaire de lire le fichier une fois entièrement pour avoir les statistiques. Mais dans cette implémentation on fait confiance au cache de l'OS en relisant entièrement le fichier 2 fois.
* Pour la méthode de diffusion d'erreur : il est nécessaire de traiter les lignes 2 par 2, mais à chaque ligne lu la mémoire est purgée.

Quelques variables sont passées dans le scope global pour simplifier la lecture du code comme `fout` ou la taille de l'image.

La lecture du header est implémentée mais toutes les informations ne sont pas entièrement utilisées :
* les images binaires ne sont pas acceptés car pas implémenté.
* si l'image est en couleur parce qu'il n'est implémenté que dans la quantification uniforme (où c'est implicite).

Des améliorations possibles : la lecture d'images écrites en binaire ou couleurs, les chemins des fichiers d'entrées/sorties ou la valeur de quantification passée en argument du programme.
