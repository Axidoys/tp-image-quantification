
#ifndef PGM_LG_COMMON_H
#define PGM_LG_COMMON_H

#define SCALE_FACTOR 16

int emulateLargeScale(int vSmall);
void writePixel(int pixelValue);

#endif //PGM_LG_COMMON_H
