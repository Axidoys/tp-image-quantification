#ifndef PGM_LG_FILEFLOW_H
#define PGM_LG_FILEFLOW_H

#include <stdio.h>

void parseImage(char filename[], char outputFilename[]);
FILE * getOutputFile(char outputFilePath[]);

#endif //PGM_LG_FILEFLOW_H
