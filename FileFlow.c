#define  _GNU_SOURCE
#include "FileFlow.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

extern void (*functionPerPixel)(int v, FILE* fout);

FILE * fout;

int pxLineBuffer[256];

int dynRange=-1;

int imageWidth=-1;
int imageHeight=-1;

//from https://stackoverflow.com/questions/735126/are-there-alternate-implementations-of-gnu-getline-interface/735472#735472
//cross-plateforme getline
size_t getline(char **lineptr, size_t *n, FILE *stream) {
    char *bufptr = NULL;
    char *p = bufptr;
    size_t size;
    int c;

    if (lineptr == NULL) {
        return -1;
    }
    if (stream == NULL) {
        return -1;
    }
    if (n == NULL) {
        return -1;
    }
    bufptr = *lineptr;
    size = *n;

    c = fgetc(stream);
    if (c == EOF) {
        return -1;
    }
    if (bufptr == NULL) {
        bufptr = malloc(128);
        if (bufptr == NULL) {
            return -1;
        }
        size = 128;
    }
    p = bufptr;
    while (c != EOF) {
        if ((p - bufptr) > (size - 1)) {
            size = size + 128;
            bufptr = realloc(bufptr, size);
            if (bufptr == NULL) {
                return -1;
            }
        }
        *p++ = c;
        if (c == '\n') {
            break;
        }
        c = fgetc(stream);
    }

    *p++ = '\0';
    *lineptr = bufptr;
    *n = size;

    return p - bufptr - 1;
}

FILE *getInputFile(char filename[]) {
    FILE* fp = fopen(filename, "r");
    if (fp == NULL) {
        perror("Can't open input file");
        exit(EXIT_FAILURE);
    }
    return fp;
}

void closeFile(FILE *fp, char *line) {
    fclose(fp);
    if (line)
        free(line);
}

FILE *getOutputFile(char outputFilePath[]) {
    if(outputFilePath == NULL){
        return NULL;
    }

    FILE* fp = fopen(outputFilePath, "w");
    if (fp == NULL) {
        perror("Can't open output file");
        exit(EXIT_FAILURE);
    }
    return fp;
}

int fillPxBuffer(char *line) {
    int col = 0;
    int octet;
    char *caret = line;

    while(caret != NULL) { //if **caret != '\n'
        if (*caret == '\n') { //TODO notDRY with below, for when there is a space before LF
            pxLineBuffer[col] = -1;
            return 1;
        }

        octet = atoi(caret);

        pxLineBuffer[col] = octet;
        col ++;

        //moveToNewSpace_AndExitIfEOL //TODO should be refactored
        while(*caret != ' ') {
            if (*caret == '\n') {
                pxLineBuffer[col] = -1;
                return 1;
            }
            caret++;
        }
        caret ++;
    }
    return 0;
}

bool isCoordinates(char *line) {
    return sscanf(line, "%i %i", &imageWidth, &imageHeight) == 2;
}

bool isDynamic(char *line, int *dynamic) {
    *dynamic = (int)strtol(line, NULL, 10);
    return (*dynamic)!=0;
}

void handleHeaders(FILE *fp) {//flush headers
    char * line = NULL;
    size_t len = 0;

    if((getline(&line, &len, fp)) == -1 || line[0] != 'P' || !(line[1] == '2' || line[1] == '3')){
        perror("Plain PGM/PPM file, first line with P2 or P3 expected");
        exit(1);
    }
    bool isGrayScale = (line[1] == '2'); //in fact not used
    if(fout != NULL){
        fprintf(fout, line);
    }

    if((getline(&line, &len, fp)) == -1 || !isCoordinates(line)){
        perror("Can't recover the size of the image");
        exit(1);
    }
    if(fout != NULL){
        fprintf(fout, line);
    }

    if((getline(&line, &len, fp)) == -1 || !isDynamic(line, &dynRange)){
        perror("Can't recover the size of the quantification");
        exit(1);
    }
    if(fout != NULL){
        fprintf(fout, line);
    }
    //end of headers

}

void parseImage(char filename[], char outputFilename[]) {
    //init
    FILE * fp;
    //FILE * fout; //is declared as global now
    fp = getInputFile(filename);
    fout = getOutputFile(outputFilename);

    //header
    handleHeaders(fp);

    //after header : work on body of the file
    char * line = NULL;
    size_t len = 0;
    while (getline(&line, &len, fp) != -1) {

        if(fillPxBuffer(line) == 0) {
            exit(-2);
        }

        for(int i = 0; i<256 && pxLineBuffer[i]!=-1 ; i++){
            (functionPerPixel)(pxLineBuffer[i], fout);
        }
    }

    closeFile(fp, line);
    closeFile(fout, line);
}