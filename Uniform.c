#include <stdio.h>
#include <math.h>
#include "Uniform.h"
#include "Common.h"

int uniformScale(int v){
    return (int)roundf((float)v / (float)(SCALE_FACTOR)); //C division truncs, we do prefer rounding for quantification
}

void uniform_functionPerPixel(int v) {
    int newVal = emulateLargeScale( uniformScale(v) );

    writePixel(newVal);
}
