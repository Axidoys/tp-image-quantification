#include <stdbool.h>
#include <malloc.h>
#include <stdlib.h>
#include "Adaptative.h"
#include "Common.h"

#define FINAL_DYNAMIC 16 //TODO should use SCALE_FACTOR in order to compare easily, compute it with macro

extern int dynRange;
extern int imageWidth;
extern int imageHeight;

int* statsTable; //table of size dynRange
int mediansAdaptedScale[FINAL_DYNAMIC];

void adaptative_computeStats(){
    const float sectorSize = (float)(imageWidth*imageHeight)/(float)(FINAL_DYNAMIC);

    int sectorCounter = 0;
    mediansAdaptedScale[0] = 0;
    float pixelCounters = 0.0f; //float in order to distribute error when sectorSize is not integer
    for(int i = 0; i < dynRange; i++){
        pixelCounters += (float)(statsTable[i]);
        if(pixelCounters>sectorSize){
            sectorCounter ++; //if passing more than one sector ? Can't do nothing without a complex logic like blurring
            pixelCounters -= (float)sectorSize;
            mediansAdaptedScale[sectorCounter] = i; //Could apply a shifting offset on post-processing
        }

    }

    //Could complete the array mediansAdaptedScale, but should not happens
    //mediansAdaptedScale[FINAL_DYNAMIC-1] = 255;
}

void adaptative_fillStats(int v) {
    if(statsTable == NULL){
        statsTable = malloc(sizeof(int) * dynRange);
        for(int i = 0; i<dynRange+1 ; i++){
            statsTable[i] = 0;
        }
    }
    statsTable[v] ++;
}

void emulateLargeScaleTable(int* table){
    int indexInAdaptedScale = 0;
    int v = mediansAdaptedScale[indexInAdaptedScale];
    table[0] = 0;
    for(int i = 1; i <= dynRange; i++){
        if(i>v) {
            indexInAdaptedScale++;
            if(indexInAdaptedScale>FINAL_DYNAMIC-1){
                v = dynRange;
            }else {
                v = mediansAdaptedScale[indexInAdaptedScale];
            }
        }
        table[i] = v;
    }
}

void adaptative_functionPerPixel(int v) {
    static int* emulatedLargeScale = NULL;
    if (emulatedLargeScale == NULL) {
        emulatedLargeScale = malloc(sizeof(int) * dynRange);
        emulateLargeScaleTable(emulatedLargeScale);
    }

    int newVal = emulatedLargeScale[v];

    writePixel(newVal);
}