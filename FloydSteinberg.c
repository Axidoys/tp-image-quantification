#include <malloc.h>
#include "FloydSteinberg.h"
#include "Common.h"

#define COEF1 16.0f/16.0f
#define COEF2 0.0f/16.0f
#define COEF3 0.0f/16.0f
#define COEF4 0.0f/16.0f


extern int imageWidth;
extern int imageHeight;

float* rowTop;
float* rowBot;

int rowIndex = 0;

void readNewPixel(int c, int v) {
    rowBot[c] = (float)v;
}

void initLinesMemory(){
    rowTop = malloc(sizeof(float) * imageWidth);
    rowBot = malloc(sizeof(float) * imageWidth);
}

void processNewLine() {
    if(rowIndex == 0){
        return; //we don't process at the first line
    }

    for(int columnIndex = 0; columnIndex < imageWidth ; columnIndex++) {
        float newVal = (float)rowTop[columnIndex] / (float)SCALE_FACTOR; //I hope (float)SCALE_FACTOR is bufferized by compiler

        //error diffusion
        int newValRound = (int)newVal;
        float error = (newVal-(float)newValRound)*(SCALE_FACTOR);//multiply scale factor because we emulate a range of 255 at the end
        rowBot[columnIndex - 1] += COEF2 * error;
        rowBot[columnIndex] += COEF3 * error;
        if(columnIndex < imageWidth - 1) {
            rowTop[columnIndex + 1] += COEF1 * error;
            rowBot[columnIndex + 1] += COEF4 * error;
        }

        writePixel(emulateLargeScale(newValRound));
    }
}

void rolloutLines() {
    float* tempAddress = rowTop;
    rowTop = rowBot;
    rowBot = tempAddress;
}

void floydSteinberg_functionPerPixel(int v) {
    //int newVal = emulateLargeScale( uniformScale(v) );
    static int columnIndex = -1;
    if(columnIndex == -1){
        initLinesMemory();
        columnIndex = 0;
    }

    readNewPixel(columnIndex, v);

    columnIndex ++;
    if(columnIndex >= imageWidth){
        processNewLine();
        rolloutLines();

        columnIndex = 0;
        rowIndex ++;

        if(rowIndex >= imageHeight){ //we are at the last pixel, need to process the bottom row
            processNewLine();
        }
    }
}
