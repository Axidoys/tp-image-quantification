#ifndef PGM_LG_ADAPTATIVE_H
#define PGM_LG_ADAPTATIVE_H

void adaptative_computeStats();
void adaptative_fillStats(int v);
void adaptative_functionPerPixel(int v);

#endif //PGM_LG_ADAPTATIVE_H
